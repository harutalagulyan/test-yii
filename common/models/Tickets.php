<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tickets".
 *
 * @property int $id
 * @property string $title
 * @property float|null $price
 * @property string $image
 * @property string|null $description
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['price'], 'number'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['image'], 'required',  'on' => 'create'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'price' => 'Price',
            'image' => 'Image',
            'description' => 'Description',
        ];
    }

    public function getImageDir(){
        return Yii::getAlias('@image_root') .'/tickets/';
    }

    public function getImage(){
        if ($this->image) {
            return Yii::getAlias('@image') . '/tickets/' . $this->image;
        }else{
            return Yii::getAlias('@image') . '/noimage.png';
        }
    }

}
