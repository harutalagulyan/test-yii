<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form of `common\models\Order`.
 */
class OrderSearch extends Order
{
    public $title;
    public $price;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ticket_id'], 'integer'],
            [['user_name', 'title', 'price'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();
        $query->joinWith('ticket');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        $dataProvider->sort->attributes['title'] = [
            'asc'  => ['tickets.title' => SORT_ASC],
            'desc' => ['tickets.title' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['price'] = [
            'asc'  => ['tickets.price' => SORT_ASC],
            'desc' => ['tickets.price' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ticket_id' => $this->ticket_id,
        ]);

        $query->andFilterWhere(['like', 'user_name', $this->user_name])
        ->andFilterWhere(['like', 'tickets.title', $this->title])
        ->andFilterWhere(['like', 'tickets.price', $this->price]);
        return $dataProvider;
    }
}
