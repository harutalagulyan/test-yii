<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Admin</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?php if (\Yii::$app->user->isGuest) : { ?>
            <?php return Yii::$app->response->redirect('/admin/site/login')?>
        <?php } else: { ?>

            <?= dmstr\widgets\Menu::widget(
                [
                    'items' => [
                        ['label' => 'Menu', 'options' => ['class' => 'header']],

                        ['label' => 'Tickets', 'icon' => 'envelope', 'url' => ['/tickets']],
                        ['label' => 'Orders', 'icon' => 'shopping-cart', 'url' => ['/order']],
                    ],
                ]
            ) ?>

        <?php }endif; ?>
    </section>
</aside>

