<?php

/* @var $this yii\web\View */
/* @var $model \common\models\Tickets */

use yii\helpers\Html;

?>

<div class="col-lg-offset-0 col-lg-4 col-md-offset-0 col-md-4 col-sm-offset-0 col-sm-6 col-xs-6 col-lg-offset-1 col-exs-10 ticket-item">
    <div class="ticket-image-wrapper">
        <div class="ticket-name">
            <?= Html::encode($model->title) ?>
        </div>
        <img src="<?=$model->getImage()?>" alt="<?= Html::encode($model->title) ?>" class="ticket-image">
    </div>
    <div class="ticket-info">
        <div class="pull-left price-wrapper">
            <span class="dollar-icon">$</span>
            <span class="price"><?=$model->price?></span>
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="description">
            <?=$model->description?>
        </div>
        <div class="bottom-info">
            <div class="button-wrapper button-order">
                <a href="<?=\yii\helpers\Url::to(['site/order', 'id' => $model->id])?>" class="buy-button">Купить</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>