<?php

/* @var $this yii\web\View */

use yii\widgets\ListView;

$this->title = 'My Yii Application';
?>
<div class="tickets-header">
    <p style="text-align:center"><span style="font-size:24px"><strong>Получи лучшие курсы от самых именитых тренеров!</strong></span></p>
</div>
<div class="tickets-wrapper">
    <div class="tickets-list">
        <div class="row">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_ticket',
                'summary'=>''
            ]);?>
        </div>
    </div>
</div>