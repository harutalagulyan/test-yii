<?php
namespace frontend\components;
class Helpers{
    public static function pr($var,$die = false){
        echo '<pre>';
        print_r($var);
        echo '</pre>';
        if(!$die) {
            die;
        }
    }
}
?>