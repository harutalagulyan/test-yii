<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order}}`.
 */
class m210323_172229_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'ticket_id' => $this->integer(),
            'user_name' => $this->string()->notNull(),
        ]);

        $this->createIndex('idx_order_ticket_id_ticket_id', '{{%order}}', 'ticket_id');
        $this->addForeignKey(
            'fk_order_ticket_id_ticket_id',
            '{{%order}}',
            'ticket_id',
            '{{%tickets}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey('fk_order_ticket_id_ticket_id', '{{%order}}');
        $this->dropIndex('idx_order_ticket_id_ticket_id', '{{%order}}');
        $this->dropTable('{{%order}}');
    }
}
