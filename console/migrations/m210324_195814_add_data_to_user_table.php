<?php

use yii\db\Migration;

/**
 * Class m210324_195814_add_data_to_user_table
 */
class m210324_195814_add_data_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $password = '$2y$13$IwoiBBWyxtV48hapGOCnDO.d4qFY7YTsbe/v4KfnBHOTaGq9EHJoe';
        $this->execute("insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`,`verification_token`) values (1,'admin','bI1-iMFc0arCpiPfyyVTzJhWSCxjGTDx','$password',NULL,'admin@admin.com',10,1616506923,1616506923,'cF9LNh-VmIkC5GwQ-aDihY-jjbbL9GuY_1616506923');    ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210324_195814_add_data_to_user_table cannot be reverted.\n";

        return false;
    }
}
