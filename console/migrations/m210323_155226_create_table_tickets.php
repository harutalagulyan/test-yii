<?php

use yii\db\Migration;

/**
 * Class m210323_155226_create_table_tickets
 */
class m210323_155226_create_table_tickets extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%tickets}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'price' => $this->decimal(10, 2)->defaultValue(0),
            'image' => $this->string()->notNull(),
            'description' => $this->text(),

        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%tickets}}');
    }
}
